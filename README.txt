Assignment 1

This code impliments an incomplete version of the card game known as 
'siete y medio' in order to show that I have learned how to commit 
changes with messages, make new branches to experiment with new features,
 merge branches together, and clean up my repository so that only the 
necessary files are present.

All changes I made to the starting files are documented in my commits 
to this repository.

I found this assignment to be incredibly useful. My confidence with Git is
much higher after doing it. I am confident that I would be able to implement
the rest of the card game, but at this point it is review for me since I am
familiar with the relevant topics from 10A and 10B.