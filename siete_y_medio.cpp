#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"
using namespace std;

// Global constants (if any)
const int DECK_SIZE = 40;
const int STARTING_MONEY = 100;

// Non member functions declarations (if any)


// Non member functions implementations (if any)


// Stub for main
int main(){
    // Seed rng off time
    srand (time(NULL));
    
    // Initialize file output
    ofstream fout;
    fout.open("gamelog.txt");
    
    // Initialize the deck, the dealer's hand, and the Player
    Hand deck(DECK_SIZE);
    Hand dealer;
    Player player(STARTING_MONEY);
    
    for (int i = 1; player.get_money() > 0; i++){
        // Display money and input bet
        cout << "You have $" << player.get_money() << ". Enter bet: ";
        int bet;
        cin >> bet;
        
        // Give the player and dealer a card from the deck
        player.deal_card(deck.draw_card());
        dealer.add_card(deck.draw_card());
        
        cout << "Your cards:\n";
        vector<Card> yourCards = player.get_cards();
        for (int i = 0; i < yourCards.size(); i++){
            Card current = yourCards[i];
            cout << "\t";
            current.print_spanish();
            cout << "\t(";
            current.print_english();
            cout << ").\n";
        }
        
        cout << "Your total is " << player.get_hand_value() << ". ";
        cout << "Do you want another card (y/n)? ";
        char prompt;
        cin >> prompt;
    }
    
    return 0;
}
